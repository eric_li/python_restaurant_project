Pizza = {"House Special" : 19.99, "Thai Dye" : 14.99, "Holy Shitake" : 21.00}
Salad = {"House Salad" : 7.99, "Greek Salad" : 7.99, "Ceasar Salad" : 9.99}
Burger = {"Cheese Burger" : 8.99, "Bacon Burger" : 9.99, "My Happy Burger" : 12.99}
Drink = {}
Drink["Soft Drink(Free Refill)"] = 2.99
Drink["Domestic Beer"] = 1.99
Drink["European Beer"] = 2.99

table1 = ["table 1", ["Guess 1", "House Special", "Ceasar Salad", "Domestic Beer"]]
table2 = ["table 2", ["Guess 1", "Bacon Burger", "Ceasar Salad", "Soft Drink(Free Refill)"],
["Guess 2", "House Special", "Domestic Beer"],
["Guess 3", "Holy Shitake", "Ceasar Salad", "Domestic Beer"]]
table3 = ["table 3", ["Guess 1", "Holy Shitake", "European Beer"],
["Guess 2", "House Special", "Ceasar Salad", "Soft Drink(Free Refill)"]]
table4 = ["table 4", ["Guess 1", "Bacon Burger", "Ceasar Salad", "Soft Drink(Free Refill)"],
["Guess 2", "House Special", "Domestic Beer"],
["Guess 3", "Holy Shitake", "Ceasar Salad", "Domestic Beer"]]
table5 = ["table 5", ["Guess 1", "Cheese Burger", "Ceasar Salad", "Soft Drink(Free Refill)"],
["Guess 2", "House Special", "Domestic Beer"],
["Guess 3", "Holy Shitake", "Ceasar Salad", "Domestic Beer"],
["Guess 4", "Cheese Burger", "Ceasar Salad", "Soft Drink(Free Refill)"],
["Guess 5", "House Special", "Soft Drink(Free Refill)"],
["Guess 6", "Holy Shitake", "Ceasar Salad", "Domestic Beer"]]
table6 = ["table 6", ["Guess 1", "Cheese Burger", "Ceasar Salad", "Soft Drink(Free Refill)"],
["Guess 2", "Thai Dye", "My Happy Burger" "Domestic Beer"],
["Guess 3", "Holy Shitake", "Ceasar Salad", "Domestic Beer"],
["Guess 4", "Cheese Burger", "Ceasar Salad", "Soft Drink(Free Refill)"],
["Guess 5", "House Special", "Soft Drink(Free Refill)"],
["Guess 6", "House Special", "Ceasar Salad", "Domestic Beer", "European Beer"]]
table7 = ["table 7", ["Guess 1", "House Special", "Ceasar Salad", "Domestic Beer"]]
table8 = ["table 8", ["Guess 1", "Bacon Burger", "Ceasar Salad", "Soft Drink(Free Refill)"],
["Guess 2", "House Special", "Domestic Beer"],
["Guess 3", "Holy Shitake", "Ceasar Salad", "Domestic Beer"]]
table9 = ["table 9", ["Guess 1", "Holy Shitake", "European Beer"],
["Guess 2", "House Special", "Ceasar Salad", "Soft Drink(Free Refill)"]]
table10 = ["table 10", ["Guess 1", "Bacon Burger", "Ceasar Salad", "Soft Drink(Free Refill)"],
["Guess 2", "House Special", "Domestic Beer"],
["Guess 3", "Holy Shitake", "Ceasar Salad", "Domestic Beer"]]

def doCheckSplit(table):
  print(table[0])
  total = 0
  for x in range(1, len(table)):
    print("\t", table[x][0], ":")
    i = 1
    while (i < len(table[x])):
      if (Pizza.get(table[x][i]) != None):
        print("\t"*2, table[x][i], " ===> ", Pizza.get(table[x][i]))
        total = total + Pizza.get(table[x][i])
      elif (Salad.get(table[x][i]) != None):
        print("\t"*2, table[x][i], " ===> ", Salad.get(table[x][i]))
        total = total + Salad.get(table[x][i])
      elif (Burger.get(table[x][i]) != None):
        print("\t"*2, table[x][i], " ===> ", Burger.get(table[x][i]))
        total = total + Burger.get(table[x][i])
      elif (Drink.get(table[x][i]) != None):
        print("\t"*2, table[x][i], " ===> ", Drink.get(table[x][i]))
        total = total + Drink.get(table[x][i])
      i = i+1
    print ("\n", "\t"*2, "Sub-Total ===> %.2f" % (total))
    total = 0

def printCheck(table):
  print(table[0])
  total = 0
  for x in range(1, len(table)):
    i = 1
    while (i < len(table[x])):
      if (Pizza.get(table[x][i]) != None):
        print("\t", table[x][i], " ===> ", Pizza.get(table[x][i]))
        total = total + Pizza.get(table[x][i])
      elif (Salad.get(table[x][i]) != None):
        print("\t", table[x][i], " ===> ", Salad.get(table[x][i]))
        total = total + Salad.get(table[x][i])
      elif (Burger.get(table[x][i]) != None):
        print("\t", table[x][i], " ===> ", Burger.get(table[x][i]))
        total = total + Burger.get(table[x][i])
      elif (Drink.get(table[x][i]) != None):
        print("\t", table[x][i], " ===> ", Drink.get(table[x][i]))
        total = total + Drink.get(table[x][i])
      i = i+1
  print ("\n", "\t", "Total ===> %.2f" % (total))

tableList = [table1, table2, table3, table4, table5, table6, table7, table8, table9, table10]
index = 0
while (index < 10):
  doCheckSplit(tableList[index])
  printCheck(tableList[index])
  index += 1
